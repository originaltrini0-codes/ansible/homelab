# Homelab Infrastructure configurations

Thanks for looking!
Here I am going through the steps to install [Ansible](https://docs.ansible.com/ansible/latest/index.html) and build out my homelab.

Here, I am using a dedicated Ubuntu host as the [control node](https://docs.ansible.com/ansible/latest/network/getting_started/basic_concepts.html#control-node).

### Installation
- Install [Ansible](https://formulae.brew.sh/formula/python@3.12#default)
```shell
sudo apt update && sudo apt install --yes ansible
```
- Validate that ansible has been installed
```shell
ansible --version
```

### Define variables
- Determine a few variables to complete this configuration
```shell
export ANSIBLE_USERNAME="<ansible-username>"
export ANSIBLE_KEYPATH="~/.ssh/${ANSIBLE_USERNAME}"
```

### SSH Setup
- Log into your Ansible control host
- Generate an SSH key pair for Ansible and accept the defaults
```shell
ssh-keygen -t ed25519 -f $ANSIBLE_KEYPATH
```

### Bootstrap a remote host
Now let me configure an unmanaged server so that Ansible can execute playbooks against
- Clone this repository using git
```shell
git clone https://gitlab.com/originaltrini0-codes/infra-configuration/homelab.git && cd ./homelab
```

- Now lets run this Ansible bootstrap script against the unmanaged server. This step assumes your user account authentication to the unmanaged server is via SSH keys. If using passwords, replace the `--private-key` flag for `--ask-become-pass`
```shell
ansible-playbook site.yaml \
  --inventory <unmanaged-server-name_or_ip-address>, \
  --user <your-username-on-unmanaged-server> \
  --private-key ~/.ssh/<ssh-filename> \
  --become --tags bootstrap \
  --extra-vars "{ansible_admin_user: $ANSIBLE_USERNAME, ansible_user_ssh_key_source: ${ANSIBLE_KEYPATH}.pub}"
```

### Configuration
- Create a disabled ansible.cfg file
```shell
ansible-config init --disabled > ansible.cfg
```
- Modify the ansible.cfg file
```shell
sed -i -r \
  's,^;?(inventory=).*$,\1./inventory/hosts.yaml,' \
  ansible.cfg;

sed -i -r \
  "s,^;?(remote_user=).*$,\1${ANSIBLE_USERNAME}," \
  ansible.cfg;

sed -i -r \
  "s,^;?(private_key_file=).*$,\1${ANSIBLE_KEYPATH}," \
  ansible.cfg;

sed -i -r \
  "s,^;?(interpreter_python=).*$,\1auto_silent," \
  ansible.cfg;
```
- Populate endpoints into `./inventory/hosts.yaml`
- Review configuration and test functionality
```shell
# Validate file paths and supporting applications
ansible --version

# Validate configuration
ansible-config dump --changed-only

# Validate your inventory file
ansible-inventory --inventory inventory/hosts.yaml --graph

# Validate authentication and communication with remote hosts
ansible all --module-name ping
```